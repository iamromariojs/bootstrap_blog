<?php
/**
 * The template for displaying all single posts.
 *
 * @package Bootstrap Blog
 */

get_header(); ?>
<?php $sidebar_position = get_theme_mod( 'blog_post_layout', 'sidebar-right' ); ?>
<?php  $view = get_theme_mod( 'blog_post_view', 'grid-view' ); ?>
<?php
  $width_class = 'col-sm-9';
  $width_class_sidebar = 'col-sm-3';
  if( $sidebar_position == 'no-sidebar' ) {
    $width_class = 'col-sm-12';
  } elseif($sidebar_position == 'sidebar-both'){
    $width_class = 'col-sm-8';
    $width_class_sidebar = 'col-sm-2';
  } 
?>

<div class="inside-page">
  <div id="container">
    <div class="row"> 
    <?php if( $sidebar_position == 'sidebar-both' || ($sidebar_position == 'sidebar-left' && is_active_sidebar( 'sidebar-left' )) ) : ?>
        <div class="<?php echo esc_attr($width_class_sidebar); ?>"><?php dynamic_sidebar( 'sidebar-left' ); ?></div>
      <?php endif; ?>  
      <div class="<?php echo esc_attr($width_class); ?>">
        <section class="page-section">
          <div class="detail-content">
            <?php while ( have_posts() ) : the_post(); ?>                    
              <?php get_template_part( 'template-parts/content', 'single' ); ?>
            <?php endwhile; // End of the loop. ?>
            <?php comments_template(); ?>

          </div><!-- /.end of deatil-content -->
        </section> <!-- /.end of section -->  
      </div>

       <?php if($sidebar_position == 'sidebar-left' || $sidebar_position == 'sidebar-both') : ?>
        <div class="<?php echo esc_attr($width_class_sidebar); ?>"><?php get_sidebar(); ?></div>
      <?php endif; ?>  
    </div>
  </div>
</div>

<?php get_footer();