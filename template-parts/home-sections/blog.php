<?php 
$home_sidebar_enabled = false;
$sidebar_position = get_theme_mod( 'blog_post_layout', 'sidebar-right' ); ?>
<?php
	$width_class = 'col-sm-8';
	if( $sidebar_position == 'no-sidebar' || !$home_sidebar_enabled ) {
		$width_class = 'col-sm-12';
	}
?>
<div class="content-area">
<div class="container">
	<div class="row">
		<?php if( $home_sidebar_enabled && ($sidebar_position == 'sidebar-left' || $sidebar_position == 'sidebar-both') && is_active_sidebar( 'sidebar-left' ) ) : ?>
			<div class="col-sm-2"><?php dynamic_sidebar( 'sidebar-left' ); ?></div>
		<?php endif; ?>
		<div class="<?php echo esc_attr( $width_class ); ?>">
			<?php get_template_part( 'template-parts/home-sections/slider', '' ); ?>
			<?php get_template_part( 'template-parts/home-sections/post-categories', '' ); ?>
			<?php get_template_part( 'template-parts/home-sections/pages', '' ); ?>
			<?php get_template_part( 'template-parts/home-sections/archive', '' ); ?>
		</div>
		<?php if($home_sidebar_enabled &&( $sidebar_position == 'sidebar-right' || $sidebar_position == 'sidebar-both' )) : ?>
			<div class="col-sm-2"><?php get_sidebar(); ?></div>
		<?php endif; ?>
	</div>
</div>
</div>