


  <div class="home-pages">
    <div class="row">
      <?php $args = [
        'orderby' => 'name',
      ];?>
      <?php foreach (get_categories($args) as $category): ?>
      
          <?php $image = "https://via.placeholder.com/350x350" ?>
          <?php 
          // The Query
          $post_args = [
            'posts_per_page' => 1 ,
            'cat' => $category->cat_ID
          ];
          $the_query = new WP_Query( $post_args );
          // The Loop to get first post thumbnail for this category
          if ( $the_query->have_posts() ) {
            while ( $the_query->have_posts() ) {
              $the_query->the_post();
              $image = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), 'medium' );
            }
            /* Restore original Post Data */
            wp_reset_postdata();
          } else {
            // no posts found
          }
          ?>
          <div class="col-sm-4 category-item">
            <div class="home-pages-block">
            <a href="<?php echo esc_url( get_category_link($category->cat_ID) ); ?>">
              <?php if( ! empty( $image ) ) : ?> <img src="<?php echo esc_url( $image[0] ); ?>"> <?php endif; ?>
            
            <div class="page-home-summary">
              </div>
              <h5 class="category-title"><?php echo $category->name ?></h5>
              </a>
            </div>
          </div>
          <?php  wp_reset_postdata(); ?>

      <?php endforeach; ?>
    </div>
  </div>