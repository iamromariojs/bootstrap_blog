<?php
/**
 * Template part for displaying posts.
 *
 * @package Bootstrap Blog
 */
?>

<?php $post_details = get_theme_mod( 'blog_post_show_hide_details', array( 'date', 'categories', 'tags' ) ); ?>
<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>  >
<a href="<?php echo esc_url( get_permalink() ); ?>" >
<div class="news-snippet" style="background-image: url(<?php echo esc_url(get_the_post_thumbnail_url(get_the_ID(), 'full' )); ?>)">        

<div class="news-summary"><?php the_title(); ?></div>
</div>
</a>    
</div>
