<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package Bootstrap Blog
 */

?>
	<footer class="main">
		<div class="container">
		<?php dynamic_sidebar( 'footer-1' ); ?>
	</div>
	</footer>
		<div class="copyright text-center">
			<?php esc_html_e( "todos os direitos reservados", 'bootstrap-blog' ); ?> <a href="<?php echo esc_url( '#' ); ?>"><?php esc_html_e( "", 'bootstrap-blog' ); ?></a> | <?php esc_html_e( 'Desenvolvido por', 'bootstrap-blog' ); ?> <a href="<?php echo esc_url( 'd1site.com.br' ); ?>"><?php esc_html_e( 'D1site','bootstrap-blog' ); ?></a>
		</div>
		<div class="scroll-top-wrapper"> <span class="scroll-top-inner"><i class="fa fa-2x fa-angle-up"></i></span></div> 
		
		<?php wp_footer(); ?>
	</body>
</html>